import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Question from "../views/Question";
import Users from "../views/Users";
import Categories from "../views/Categories";
import Questions from "../views/Questions";

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/question/:id',
    name: 'Question',
    component: Question,
  },
  {
    path: '/users',
    name: 'Users',
    component: Users,
  },
  {
    path: '/categories',
    name: 'Categories',
    component: Categories,
  },
  {
    path: '/questions',
    name: 'Questions',
    component: Questions,
  },
]

const router = new VueRouter({
  routes,
  mode: 'history'
})

export default router
